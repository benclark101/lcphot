# LCPhot

A photometry routine that's specifically geared towards high-precision time-series photometry. 

## Requirements

Python 2.7
You'll also need the following libraries:
* uncertainties 
* pandas
* datetime
* pyfits
* numpy
* photutils
* matplotlib
* sep
* uncertainties
* pandas
* lmfit 
* astropy
* scipy 
* signal


## Usage

Download the raw python file to a destination of your choosing, and edit the "User Variables" section at the top of the .py file:

```############ USER VARIABLES ############
fits_directory="/data/1_imaging/cfht/w36/cal/w36_cal/nanfix/det1/"
output_dir = "w36_phot_out_test_man/" #output file directory
target = (112,1016) #(X,Y) of target star
max_stars = 10#maxiumum number of stars to measure
ref_image="1674427o_04.fits" #image used for source detection
star_number_add=0 #addition to star count for other detectors i.e. start on star 8 instead of 1
#Aperture Setup
ap_min = 6 #Lowest aperture size
ap_max = 15 #Highest aperture size
ap_step=.5 #Gap between apertures 
annulus_inner=1.5 #sky annulus size as a multiple of aperutre size
annulus_size=10 #size of sky annulus in pixels
fwhm = 6.5 #approximate FWHM used as fallback
centering="fwc"#centering method: psf fitting (fit) or flux-weighted centroid (fwc)
#Source detection
source_list = "w36-d1.coords" #location of manual source list - False for autodetect
window_size = 18 #window size in pixels to use around PSFs for cetnroiding
#Detector Information
detector = 0 #detector within fits image for FITS that contain more than one image
key_gain="GAIN" #GAIN fits header keyword
key_time = "MJD-OBS" #TIME/DATE fits header keyword
key_airm="AIRMASS" #Airmass fits header keyword
key_exp="EXPTIME" #Exposure time fits header keyword
#Runtime
debug = False
rerun = True
bgsig=6
#FWHM Variation
# fwhm_cor = ["None","Linear","Squared"];
# fw_arr=[1,fwhm_dif,fwhm_dif*fwhm_dif]
fwhm_cor = ["None"];
fw_arr=[1]

```

