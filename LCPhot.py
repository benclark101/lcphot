#Photometry code
#b.j.clark@keele.ac.uk

# Libraries ####################################################################
from standard import *
from uncertainties import ufloat,unumpy
import pandas
import datetime
import pyfits
import numpy
from photutils import CircularAperture,CircularAnnulus,aperture_photometry
from photutils.utils import calc_total_error
import matplotlib.pyplot as plt
import sep
from uncertainties import ufloat
import pandas
import os
from lmfit import *
from astropy.modeling import models, fitting
from scipy import ndimage, interpolate
from astropy.convolution import convolve, convolve_fft
import signal
import sys
import numpy as np

# Variables ####################################################################
#Detector 2
fits_directory="/data/1_imaging/cfht/w36/cal/w36_cal/nanfix/det1/"
output_dir = "w36_phot_out_test_man/" #output file directory
target = (112,1016) #(X,Y) of target star
max_stars = 10#maxiumum number of stars to measure
ref_image="1674427o_04.fits" #image used for source detection
star_number_add=0 #addition to star count for other detectors i.e. start on star 8 instead of 1
#Aperture Setup
ap_min = 6 #Lowest aperture size
ap_max = 15 #Highest aperture size
ap_step=.5 #Gap between apertures 
annulus_inner=1.5 #sky annulus size as a multiple of aperutre size
annulus_size=10 #size of sky annulus in pixels
fwhm = 6.5 #approximate FWHM used as fallback
centering="fwc"#centering method: psf fitting (fit) or flux-weighted centroid (fwc)
#Source detection
source_list = False #location of manual source list - False for autodetect
window_size = 18 #window size in pixels to use around PSFs for cetnroiding
#Detector Information
detector = 0 #detector within fits image for FITS that contain more than one image
key_gain="GAIN" #GAIN fits header keyword
key_time = "MJD-OBS" #TIME/DATE fits header keyword
key_airm="AIRMASS" #Airmass fits header keyword
key_exp="EXPTIME" #Exposure time fits header keyword
#Runtime
debug = False
rerun = True
bgsig=6
#FWHM Variation
# fwhm_cor = ["None","Linear","Squared"];
# fw_arr=[1,fwhm_dif,fwhm_dif*fwhm_dif]
fwhm_cor = ["None"];
fw_arr=[1]

# Functions ####################################################################
def signal_handler(signal, frame):
        print('Exiting!')
        sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

def arrays_to_file(listofarrays,filename,append=False,sepchar="\t"):
  ltemp = []
  if len(listofarrays[0]) <2:
    print ("ERROR: Check array length")
  for arr in listofarrays:
    try:
      if arr == 0:
        arr = numpy.zeros(len(listofarrays[0]))
        arr = numpy.arr(arr,dtype="int")
        arr = numpy.arr(arr,dtype="str")
    except:
      normalarray=True
    ltemp.append(arr)
  listofarrays = ltemp
  if append == True:
    fout = open(filename,"a")
  else:
    fout = open(filename,"w")
  for i in range(0, len(listofarrays[0])):
    for j in range(0,len(listofarrays)):
      fout.write(str(listofarrays[j][i]))
      if j == len(listofarrays)-1:
        fout.write("\n")
      else:
        fout.write(sepchar)
  fout.close()


def nar(array,typ="float"):
  try:
    return numpy.asarray(array,dtype=typ)
  except:
    print ("Using String array")
    return numpy.asarray(array,dtype="str")

def fits_import(img_name,dext=0,hext=0,det=False):
  import pyfits
  if det != False:
    dext,hext = det, det
  o_img = pyfits.open(img_name)
  img = o_img[dext].data
  hdr = o_img[hext].header
  img2 = img.T
  return (img2,hdr)


def is_array(var):
    return isinstance(var, (list, tuple))

def flatten_list(to_flatten,once=False):
  values = []
  for val in to_flatten:
    if is_array(val) and once == False:
      [values.append(v) for v in flatten_list(val)]
    else:
      values.append(val)
  return values

def med_abs_dev(data):
    return median(numpy.absolute(data -median(data)))

def median(array,**kwargs):
  return median(array[~numpy.isnan(array)],**kwargs)

def show_fits(img, clims="auto",show=True,newfig=False,bgsub=False,title=False,ax=False):
  img=img.T
  import matplotlib.pyplot as plt
  if newfig == False:
    fig = plt.figure()
    cmap = plt.cm.hot
    cmap.set_bad((0, 0.6, 1, 1))
    ax = fig.add_subplot(111)
  else:
      if ax == True:
          fig,ax = newfig
      else:
          fig,pos = newfig
          ax = fig.add_subplot(pos)
  if title != False:
    ax.set_title(title)
  if clims == "auto":
    if bgsub == True:
      from scipy.stats import mode
      goodpix = img[~numpy.isnan(img)]
      bad = mode(img[~numpy.isnan(img)])[0][0]
      med = median(goodpix[numpy.where(goodpix!=bad)])
      mad = med_abs_dev(goodpix[numpy.where(goodpix!=bad)])
    else:
      med = median(img[~numpy.isnan(img)])
      mad = med_abs_dev(img[~numpy.isnan(img)])
    thrh,thrl = med+(mad*10), med-(mad*10)
    if thrl < min(flatten_list(img.tolist())): thrl = min(flatten_list(img.tolist()))
    if thrh > max(flatten_list(img.tolist())): thrh = max(flatten_list(img.tolist()))
  else:
    thrh=clims[0]
    thrl=clims[1]
  cmap = plt.cm.hot
  im1 = ax.imshow(img,clim=(thrl,thrh),origin='lower',interpolation="none",cmap=cmap)
  fig.colorbar(im1)
  numrows, numcols = img.shape
  imFORMAT = img
  def format_coord(x, y):
    col = round(x)
    row = round(y)
    if col>=0 and col<numcols and row>=0 and row<numrows:
        z = imFORMAT[row,col]
        return 'x=%1.4f, y=%1.4f, count=%1.4f'%(x, y, z)
    else:
        return 'x=%1.4f, y=%1.4f'%(x, y)
  ax.format_coord = format_coord
  plt.tight_layout()
  if show == True:
    plt.show()
  else:
    return ax

def slash(flname):
	if not flname.endswith("/"):
		flname = flname+"/"
	return str(flname)

def cmdir(direc,makenew=False,fp=False):
  direc = slash(direc)
  if not os.path.exists(direc):
    os.makedirs(direc)
    if not direc.endswith("/"):
      direc +='/'
  else:
    if makenew == True:
      rerun = 0
      if direc.endswith("/"):
        direc = direc[:-1]
      directory_init = direc
      while os.path.exists(direc):
          rerun += 1
          direc = directory_init + "_"+str(rerun)
      direc +='/'
      os.makedirs(direc)
      print ("Created new directory at: ", direc)
  if not direc.startswith("/") and fp == True:
    direc = os.getcwd()+"/"+direc
  return direc


def alt_combine(list1,list2):
    result = [None]*(len(list1)+len(list2))
    result[::2] = list1
    result[1::2] = list2
    return result

def plot_ap(x,y,ax,star,clr="#62FF00"):
	circle1 = plt.Circle((x, y), 25, color=clr,alpha=0.6)
	plt.plot(x,y,"+",ms=25,color="k")
	plt.text(x+2.,y+2.,"star"+str(star),color="k")
	ax.add_artist(circle1)

def plot_sources(data,coords):
  ax = show_fits(data,show=False)
  for i in range(0,len(coords)):
    clrr = "#62FF00"
    x = coords[i][0]
    y = coords[i][1]
    plot_ap(x,y,ax,i+1,clrr)
  plt.axis(ymin=0,ymax=len(data[0]),xmin=0,xmax=len(data))
  plt.title("Sources")
  plt.show()

def get_sources(data,kernel,targ,sigma=4,show=False,debug=False,stars=False):
    data = data.copy(order='C')
    data = data.byteswap().newbyteorder()
    bkg = sep.Background(data)
    bkg_subtraced_data = data - bkg
    if show==True and debug == True:
    	show_fits(data,show=False)
    	show_fits(np.array(bkg),show=False)
    	show_fits(bkg_subtraced_data,show=False)
    to_convolve = bkg_subtraced_data.copy()
    to_convolve[numpy.where(numpy.isnan(to_convolve))] = median(to_convolve[numpy.where(~numpy.isnan(to_convolve))])
    tcM, tcMAD = median(to_convolve),med_abs_dev(to_convolve)
    result = convolve_fft(to_convolve, kernel)
    result = (((result-median(result))/med_abs_dev(result) ) * tcMAD ) + tcM
    res = result.copy(order='C')
    if sigma == "bad":
      thresh=1.
    else:
      thresh = median(res) + (sigma*med_abs_dev(res))      
    if debug==True:
      print "CONVOLVED"
      show_fits(res,show=False)
      plt.clf()
      r2 = res.copy()
      r2[numpy.where(r2<thresh)] = 0
      print "threshold:",thresh
      print r2[:10]
      show_fits(r2,show=True)
    objects = sep.extract(res, thresh, filter_type='conv')
    if debug == True:print "OBJECTS:", (len(objects))
    objects = sorted(zip(objects['flux'],objects['y'],objects['x']),reverse=True)
    if stars != False:
      objects=objects[:stars]
    source_xy = [x[1:] for x in objects]
    distances =  numpy.sum(numpy.abs(nar(source_xy) - [targ[0],targ[1]]),axis=1)
    source_i = numpy.where(distances==numpy.amin(distances))[0][0]
    t_x,t_y = source_xy[source_i][0],source_xy[source_i][1]
    skipi=False
    coordlist=[0]
    ax = show_fits(bkg_subtraced_data,show=False)
    for i in range(0,len(objects)):
        clrr = "#62FF00"
        x = objects[i][1]
        y = objects[i][2]
        if skipi==True:i=i-1
        if x == t_x and y == t_y:
            clrr="b"
            i=-1
            skipi=True
            coordlist[0]=(x,y)
        else:
            coordlist.append((x,y))
        if show == True:plot_ap(x,y,ax,i+2,clrr)
    print ("Please check sources are correct and re-run if necessary (Target in BLUE)")
    if show == True:
        plt.axis(ymin=0,ymax=len(bkg_subtraced_data[0]),xmin=0,xmax=len(bkg_subtraced_data))
        plt.title("Sources")
        plt.show()
    return coordlist

def sub_bg(data):
	data = data.copy(order='C')
	data = data.byteswap().newbyteorder()
	bkg = sep.Background(data)
	bkg_subtraced_data = data - bkg
	return bkg_subtraced_data,bkg.rms()

def get_psf(img,sx,sy,w=17,rcord=False):
    img,rms = sub_bg(img)
    img[numpy.where(img<rms*3)] = 0
    psf = img[int(sx-w):int(sx+w+1),int(sy-w):int(sy+w+1)]
    if rcord==False:
        return psf
    else:
        return (psf,sx-w,sy-w)

def median(array,**kwargs):
	gdar = array[~numpy.isnan(array)]
	return numpy.median(gdar[numpy.where(gdar!=0)],**kwargs)
def mean(array,**kwargs):
	gdar = array[~numpy.isnan(array)]
	return numpy.mean(gdar[numpy.where(gdar!=0)],**kwargs)


#----- PSF Profile Fitting ------
def profile(x, A1=1,A2=1, mu=0, fwg=1,fwl=1, width=10):
	x = numpy.linspace(numpy.amin(x)-1,numpy.amax(x),1000)
	gauss_model_l = models.Voigt1D(amplitude_L=A1, x_0=mu, fwhm_L=fwl, fwhm_G=fwg)
	gauss_model_r = models.Voigt1D(amplitude_L=A2, x_0=mu, fwhm_L=fwl, fwhm_G=fwg)
	gauss_left_x = x[numpy.where(x<=mu)]
	gauss_left = gauss_model_l(gauss_left_x)
	gauss_right_x = x[numpy.where(x>=mu)]
	gauss_right = gauss_model_r(gauss_right_x)
	gauss_left_x = gauss_left_x - (width/2)
	gauss_right_x = gauss_right_x + (width/2)
	X = numpy.append(gauss_left_x,gauss_right_x)
	Y = numpy.append(gauss_left,gauss_right)
	X = numpy.append(numpy.asarray([numpy.amin(x)]),X);X = numpy.append(X,numpy.asarray([numpy.amax(x)]));
	Y = numpy.append(numpy.asarray([0]),Y);Y = numpy.append(Y,numpy.asarray([0]));
	gauss_func = interpolate.interp1d(X,Y)
	return gauss_func

def model(free_params,x):
	func = profile(x,free_params['A1'].value,free_params['A2'].value,free_params['mu'].value,free_params['fw_g'].value,free_params['fw_l'].value,free_params['width'].value)
	return func(x)

def residuals(free_params,x,y):
	return y - model(free_params,x)

def profile_center(profile,fw_min,window_size,plot=False,rpar=False,params=False,ax=False):
    xr = numpy.arange(0,len(profile))
    if params==True:
        free_params = params
    else:
        free_params = Parameters()
        free_params.add('A1', value= max(profile))
        free_params.add('A2', value= max(profile))
        free_params.add('mu', value= len(profile)/2)
        free_params.add('fw_g', value= 3,min=0,max=10)
        free_params.add('fw_l', value= 3,min=0,max=10)
        free_params.add('width', value= 10,min=fw_min,max=window_size)
    result = minimize(residuals, free_params,args=(xr,profile),method="leastsq")
    free_params = result.params
    fwl = free_params["fw_l"].value
    fwg = free_params["fw_g"].value
    fwhm = free_params["width"].value + ( (0.5346*fwl) + numpy.sqrt( (0.2166*numpy.square(fwl)) + numpy.square(fwg)  ) )
    if plot == True:
        if ax == False:
        	plt.figure()
        	plt.plot(xr,profile,"k-")
        	plt.plot(xr,model(free_params,xr),"r-")
        	plt.axvline(free_params["mu"].value)
        else:
        	ax.plot(xr,profile,"k-")
        	ax.plot(xr,model(free_params,xr),"r-")
        	ax.axvline(free_params["mu"].value,color="k")
        	ax.axvline(free_params["mu"].value+(fwhm/2))
        	ax.axvline(free_params["mu"].value-(fwhm/2))
    if rpar==False:
        return (free_params["mu"].value,fwhm)
    else:
        return (free_params["mu"].value,fwhm,free_params)

def get_centre(img,x,y,fw_min=5,w=10,plot=False,rpar=False,params=False):
    psf,xlr,ylr = window(img,x,y,w,ret="i")
    psf = psf - median(psf)
    psf[numpy.where(psf<0)]=0
    if plot==True:
        fig = plt.figure()
        ax = fig.add_subplot(222)
        ax3 = fig.add_subplot(221)
        ax2 = fig.add_subplot(224)
        ax3.set_title("PSF Kernel")
        show_fits(psf,bgsub=True,show=False,newfig=(fig,ax),ax=ax)
        # plt.show()
    ypr = numpy.mean(psf,axis=0)
    xpr = numpy.mean(psf.T,axis=0)
    if params == True and plot==True:
        cntr_x = profile_center(xpr,fw_min,w,plot,rpar,params[0],ax=ax2)
        cntr_y = profile_center(ypr,fw_min,w,plot,rpar,params[1],ax=ax3)
    elif params == True:
        cntr_x = profile_center(xpr,fw_min,w,plot,rpar,params[0])
        cntr_y = profile_center(ypr,fw_min,w,plot,rpar,params[1])
    elif plot ==True:
        cntr_x = profile_center(xpr,fw_min,w,plot,rpar,ax=ax2)
        cntr_y = profile_center(ypr,fw_min,w,plot,rpar,ax=ax3)
    else:
        cntr_x = profile_center(xpr,fw_min,w,plot,rpar)
        cntr_y = profile_center(ypr,fw_min,w,plot,rpar)
    if plot == True: plt.show()
    if rpar==True:
        return (cntr_x[0]+xlr,cntr_y[0]+ylr,cntr_x[1],cntr_y[1],cntr_x[2],cntr_y[2])
    else:
        return (cntr_x[0]+xlr,cntr_y[0]+ylr,cntr_x[1],cntr_y[1])

def window(img,x,y,w,ret="i"):
    x_lo = x-w
    x_hi = x+w+1
    y_lo = y-w
    y_hi = y+w+1
    if x_lo < 0: x_lo=0;
    if x_hi > len(img): x_hi = len(img)
    if y_lo < 0: y_lo=0;
    if y_hi > len(img[0]): y_hi = len(img[0])
    if ret=="i":
    	return img[int(x_lo):int(x_hi),int(y_lo):int(y_hi)].copy(),x_lo,y_lo
    if ret=="v":
    	return(x_lo,x_hi,y_lo,y_hi)


def flux_weighted_centroid(img,x,y,fw_min=5,w=10,plot=False,rpar=False,params=False):
  img,xlr,ylr = window(img,x,y,w,ret="i")
  X,Y,flux = [],[],[]
  for y in range(0,len(img)):
      for x in range(0,len(img[0])):
          X.append(x)
          Y.append(y)
          flux.append(img[y][x])
  X,Y,flux = numpy.asarray(Y),numpy.asarray(X),numpy.asarray(flux)
  flux[numpy.where(numpy.isnan(flux))] = 0

  return numpy.array((numpy.average(X,weights=flux)+xlr,numpy.average(Y,weights=flux)+ylr,17,17))


# Main #########################################################################
prihdr("Photometry Pipeline")
if rerun==True:
	output_dir = cmdir(output_dir)
else:
	output_dir = cmdir(output_dir,True)
#Find Sources
targ_x, targ_y = target
ref_fl = fits_directory+ref_image
ref_im, ref_hd = fits_import(ref_fl,det=detector)
ref_im_fx = ref_im.byteswap().newbyteorder()
#Find Background
data = ref_im_fx.copy(order='C')
bkg = sep.Background(data)
ref_im[numpy.isnan(ref_im)] = bkg.back()[numpy.isnan(ref_im)]
if centering=="fwc":targ_x,targ_y,fw_ref_x,fw_ref_y = flux_weighted_centroid(ref_im,targ_x,targ_y,fw_min=5,w=window_size,plot=True)
if centering=="fit":targ_x,targ_y,fw_ref_x,fw_ref_y = get_centre(ref_im,targ_x,targ_y,fw_min=5,w=window_size,plot=True)
fwhm_ref = numpy.mean([fw_ref_x,fw_ref_y])
psf_kernel = get_psf(ref_im,targ_x,targ_y,w=window_size)
if debug==True:
	print "PSF KERNEL"
	show_fits(psf_kernel)
if source_list == False:
	print ("Finding Sources..")
	stellar_coords = get_sources(ref_im,psf_kernel,(targ_x,targ_y),show=True,debug=debug,sigma=bgsig,stars=max_stars)
	plt.savefig(output_dir+"sources.png",filetype="png",dpi=500)
	arrays_to_file(zip(*stellar_coords),output_dir+"sources.coords")
	# plt.show()
else:
	print ("Reading sources from", source_list)
	stellar_coords=zip(*readfile(source_list))
	print stellar_coords
	plot_sources(ref_im,stellar_coords)

if max_stars != False:
	stellar_coords=stellar_coords[:max_stars]

#set up apertures
aper_range = numpy.arange(ap_min,ap_max+ap_step,ap_step)
annulus_inner = annulus_inner * aper_range

file_list = get_files(fits_directory,ext="fits")#[::10]
aper_out = numpy.asarray(aper_range)
#Photometry
print ("Performing Photometry")
phot_out = open(output_dir+"phot.df","w")
phot_out.write("#"+"\t".join(["time","star","x","y","ap","an_in","an_out","counts","count_err","bg_mean","bg_mean_err","flux","flux_err","fwhm_x","fwhm_y","fwhm","fwhmd","fwhmcor","airmass","exptime","fwcorap"])+"\n")
bar = pbar(len(file_list));cnt=0;
for fl in file_list:
	#Load Data
	bar.update(cnt);cnt+=1
	img,hdr = fits_import(fits_directory+fl,det=detector)
	time=float(hdr[key_time])
	airmass=float(hdr[key_airm])
	exptime=float(hdr[key_exp])
	gain = float(hdr[key_gain])
	if not isinstance(aper_range, (list, np.ndarray)):
	    aper_range = [aper_range]
	if not isinstance(annulus_inner, (list, np.ndarray)):
	    annulus_inner = [annulus_inner]
	#Center Stars
	this_coords,this_fwhm = [],[]
	for st in stellar_coords:
		if centering=="fwc":
			x,y,fw_x,fw_y = flux_weighted_centroid(img,st[0],st[1],fw_min=5,w=window_size,plot=False)
		if centering=="fit":
			x,y,fw_x,fw_y = get_centre(img,st[0],st[1],fw_min=5,w=window_size,plot=False)
		this_coords.append((x,y))
		this_fwhm.append((fw_x,fw_y))
	fwhm_dif = numpy.mean(this_fwhm[0])/fwhm_ref
	#Mask out bad pixels
	mask = numpy.zeros_like(img, dtype=bool)
	mask[numpy.isnan(img)] = True
	#Calculate error
	bkg = img.copy(order='C')
	bkg = sep.Background(bkg.byteswap().newbyteorder())
	bkg_err = bkg.rms()
	error = calc_total_error(img, bkg_err, gain)
	#setup aperture
	if debug == True: print this_coords, "\n\n"
	fwcnt = 0;
	for fwcor in fw_arr:
		this_aper = numpy.asarray(aper_range) * fwcor
		this_ann = numpy.asarray(annulus_inner) * fwcor
		aperture = [CircularAperture(this_coords, r=ap) for ap in this_aper]
		sky_aperture=[CircularAnnulus(this_coords, r_in=an, r_out=an+annulus_size) for an in this_ann]
		apers=aperture+sky_aperture
		#Perform Photometry
		phot_table = aperture_photometry(img.T, apers,mask=mask,error=error)
		#Read Outputs
		counts = [unumpy.uarray(numpy.asarray(phot_table['aperture_sum_'+str(aa)].data,dtype="float"),numpy.asarray(phot_table['aperture_sum_err_'+str(aa)].data,dtype="float")) for aa in range(0,len(this_aper))]
		bkgs = [unumpy.uarray(numpy.asarray(phot_table['aperture_sum_'+str(aa)].data,dtype="float"),numpy.asarray(phot_table['aperture_sum_err_'+str(aa)].data,dtype="float")) for aa in range(len(this_aper),2*len(this_aper))]
		for ST in range(0,len(this_coords)):
		    fwx,fwy=this_fwhm[ST]
		    fwhm = numpy.mean([fwx,fwy])
		    for i in range(0,len(this_aper)):
		        bmg = (bkgs[i][ST]/sky_aperture[i].area())*aperture[i].area()
		        flx=counts[i][ST]-bmg
		        phot_out.write("\t".join([str(col) for col in [time,ST+1,this_coords[ST][0],
				this_coords[ST][1],aper_out[i],this_ann[i],this_ann[i]+annulus_size,counts[i][ST].n,
				counts[i][ST].s,bmg.n,bmg.s,flx.n,flx.s,fwx,fwy,fwhm,fwhm_dif,fwhm_cor[fwcnt],airmass,exptime,this_aper[i]]])+"\n")
		fwcnt += 1
phot_out.close()
bar.finish()

#Seperate outputs into star data files
dat = pandas.read_csv(output_dir+"phot.df",delimiter="\t",header=0)
dat['ap'] = dat['ap'].map(lambda x: '%.2f' % x) #float fix
print "read"
bar = pbar(len(dat.fwhmcor.unique()) * len(dat.star.unique()) * len(dat.ap.unique()) ); cnt =0;
for fwc in dat.fwhmcor.unique():
    out_top = output_dir+cmdir("fw_"+str(fwc))
    this_fwc = dat.loc[dat["fwhmcor"]==fwc]
    for st in dat.star.unique():
        out_star = cmdir(out_top+"star"+str(st))
        this_star = this_fwc.loc[this_fwc["star"]==st]
        for ap in dat.ap.unique():
            select=this_star.loc[this_star["ap"]==ap]
            select.to_csv(out_star+"star"+str(st+star_number_add)+"_"+str(ap)+"px.dat", columns=['time','flux','flux_err'],header=False,sep="\t",index=False)
            select.to_csv(out_star+"star"+str(st+star_number_add)+".dat",sep="\t")
            bar.update(cnt);cnt+=1;
bar.finish()

#Plot Apertures
show_fits(data,show=False)
apertures.plot(color="k",fill="rgb(21, 82, 164, 0.59)",alpha=0.2)
annulus_apertures.plot(color="#21b5d5")
plt.show()
